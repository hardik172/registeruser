
import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import useAxiosPost from '../hooks/useAxios';
import Label from '../components/label';
import Button from '../components/button';
import '../assets/registerPage.css';

const initialValues = {
    full_name: '',
    contact_number: '',
    email: '',
    date_of_birth: '',
    password: '',
    confirm_password: '',
};

const validationSchema = Yup.object({
    full_name: Yup.string().required('Full Name is required'),
    contact_number: Yup.string()
        .required('Contact Number is required')
        .matches(/^(?:\+?1[-.\s]?)?\(?([0-9]{3})\)?[-.\s]?([0-9]{3})[-.\s]?([0-9]{4})$/,
            "Invalid canadian contact number"
        ),
    email: Yup.string().email('Sorry, this email address is not valid. Please try again.').required('Email is required'),
    date_of_birth: Yup.date()
        .required('Date of Birth is required')
        .max(new Date(), "Date of birth must be in the past"),
    password: Yup.string().required('Password is required')
        .min(8, 'Password must be at least 8 characters')
        .matches(
            /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]){8,}/,
            'Password must contain at least one lowercase letter, one uppercase letter and one number'
        ),
    confirm_password: Yup.string()
        .oneOf([Yup.ref('password'), null], 'Passwords must match')
        .required('Confirm Password is required'),
});

const RegisterPage = () => {
    const { onSubmit, isSubmitting, status } = useAxiosPost();
    const updatedStatus = status;
    return (
        <div>
            <h1 className="h1">Create User Account</h1>
            <Formik
                initialValues={initialValues}
                validationSchema={validationSchema}
                onSubmit={onSubmit}
            >
                {({ isSubmitting }) => (
                    <Form>
                        <div>
                            <div>
                                <Label name="full_name">Full Name </Label>
                                <Field type="text" name="full_name" placeholder="Enter Full Name" ></Field>
                                <ErrorMessage name="full_name" component="div" className="error" />
                            </div>

                            <div>
                                <Label name="contact_number">Contact Number </Label>
                                <Field type="text" name="contact_number" placeholder="Enter Contact Number" />
                                <ErrorMessage name="contact_number" component="div" className="error" />
                            </div>

                            <div>
                                <Label name="email">Email </Label>
                                <Field type="email" name="email" placeholder="Enter Email" />
                                <ErrorMessage name="email" component="div" className="error" />
                            </div>

                            <div>
                                <Label name="date_of_birth">Date of Birth </Label>
                                <Field type="date" name="date_of_birth" />
                                <ErrorMessage name="date_of_birth" component="div" className="error" />
                            </div>

                            <div>
                                <Label name="password">Password </Label>
                                <Field type="password" name="password" placeholder="Create Password" />
                                <ErrorMessage name="password" component="div" className="error" />
                            </div>

                            <div>
                                <Label name="confirm_password">Confirm Password </Label>
                                <Field type="password" name="confirm_password" placeholder="Confirm Password" />
                                <ErrorMessage name="confirm_password" component="div" className="error" />
                            </div>
                        </div>

                        <Button type="cancel">Cancel</Button>
                        <Button type="submit" disabled={isSubmitting}>{isSubmitting ? 'Submitting...' : 'Submit'}</Button>

                        {updatedStatus && updatedStatus.success ?
                            <div className="success">{updatedStatus.success}</div>
                            : ''
                        }
                        {updatedStatus && updatedStatus.error ?
                            <div className="error">{updatedStatus.error}</div>
                            : ''
                        }
                    </Form>
                )}
            </Formik>
        </div >
    )
}

export default RegisterPage;
