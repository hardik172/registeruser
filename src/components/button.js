import React from "react";

const Button = ({ type, disabled, className, children }) => {
    return (
        <button type={type} disabled={disabled} className={className}> {children}</button>
    )
}

export default Button;