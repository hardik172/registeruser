import React from "react";

const Label = ({ name, className, children }) => {
    return (
        <label htmlFor={name} className={className}>
            {children}
        </label>
    )
}

export default Label;