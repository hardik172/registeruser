import { useState } from 'react';
import axios from 'axios';

const useAxiosPost = () => {
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [status, setStatus] = useState(null);

    const onSubmit = async (values, { resetForm }) => {
        setIsSubmitting(true);
        setStatus(null);

        try {
            const response = await axios.post('https://fullstack-test-navy.vercel.app/api/users/create', values);
            console.log("response.data", response.data)
            setStatus({ success: response.data.description });
            resetForm();
        } catch (error) {
            setStatus({ error: error.response.data });
        }

        setIsSubmitting(false);
    };

    return { onSubmit, isSubmitting, status };
};

export default useAxiosPost;